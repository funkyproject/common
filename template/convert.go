package main

import (
	"encoding/xml"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
)

func convert(reader io.Reader, prependPath string) ([]issue.Issue, error) {
	var doc Document

	err := xml.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	issues := make([]issue.Issue, len(doc.Messages)) // TODO
	for i, msg := range doc.Messages {
		issues[i] = issue.Issue{
			Message: msg, // TODO
		}
	}
	return issues, nil
}

type Document struct {
	Messages []string `xml:"XXX"` // TODO
}
