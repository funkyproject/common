# TODO analyzer changelog

GitLab TODO analyzer follows versioning of GitLab (`MAJOR.MINOR` only) and generates a `MAJOR-MINOR-stable` [Docker image](https://gitlab.com/gitlab-org/security-products/TODO/container_registry).

These "stable" Docker images may be updated after release date, changes are added to the corresponding section bellow.

## X-Y-stable
- Initial releas