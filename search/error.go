package search

var errNotFound = &ErrNotFound{}

type ErrNotFound struct {
	searchPath string
}

func (e ErrNotFound) Error() string {
	return "No match in " + e.searchPath
}

func (e ErrNotFound) ExitCode() int {
	return 3
}
